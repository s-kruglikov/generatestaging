﻿using System.Collections.Generic;

namespace GenerateStaging.DataAccess
{
	public class MockDatabaseExplorer : IDatabaseExplorer
	{
		public List<string> GetColumns(string tableName)
		{
			return new List<string>
			{
				"Column_Name_1",
				"Column_Name_2",
				"Column_Name_3",
				"Column_Name_4",
				"Column_Name_5",
				"Column_Name_6",
				"Column_Name_7",
				"Column_Name_8",
				"Column_Name_9",
				"Column_Name_10"
			};
		}

		public List<string> GetTables()
		{
			return new List<string>
			{
				"R_Table_Name_1",
				"R_Table_Name_2",
				"R_Table_Name_3",
				"R_Table_Name_4",
				"R_Table_Name_5",
				"R_Table_Name_6",
				"R_Table_Name_7",
				"R_Table_Name_8",
				"R_Table_Name_9",
				"R_Table_Name_10",
				"STG_Table_Name_1",
				"STG_Table_Name_2",
				"STG_Table_Name_3",
				"STG_Table_Name_4",
				"STG_Table_Name_5",
				"STG_Table_Name_6",
				"STG_Table_Name_7",
				"STG_Table_Name_8",
				"STG_Table_Name_9",
				"STG_Table_Name_10",
			};
		}
	}
}

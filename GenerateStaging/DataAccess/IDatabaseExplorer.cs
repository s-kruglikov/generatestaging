﻿using System.Collections.Generic;

namespace GenerateStaging.DataAccess
{
	/// <summary>
	/// Represents interface providing database entities information.
	/// </summary>
	public interface IDatabaseExplorer
	{
		List<string> GetTables();

		List<string> GetColumns(string tableName);
	}
}

﻿using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace GenerateStaging.DataAccess
{
	public class OracleDatabaseExplorer : IDatabaseExplorer
	{
		public List<string> GetTables()
		{
			var tablesList = new List<string>();

			using (OracleConnection conn = GetConnection())
			{
				var query = "SELECT DISTINCT OBJECT_NAME FROM USER_OBJECTS WHERE OBJECT_TYPE = 'TABLE' ORDER BY 1 ASC";

				var command = new OracleCommand
				{
					Connection = conn,
					CommandText = query,
					CommandType = CommandType.Text
				};

				conn.Open();
				var oracleReader = command.ExecuteReader();

				if (oracleReader != null)
				{
					while (oracleReader.Read())
					{
						tablesList.Add(oracleReader[0].ToString());
					}
				}
			}

			return tablesList;
		}

		public List<string> GetColumns(string tableName)
		{
			var tablesList = new List<string>();

			using (OracleConnection conn = GetConnection())
			{
				var query = 
					string.Format("SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME = '{0}'", tableName.ToUpperInvariant());

				var command = new OracleCommand
				{
					Connection = conn,
					CommandText = query,
					CommandType = CommandType.Text
				};

				conn.Open();
				var oracleReader = command.ExecuteReader();

				if (oracleReader != null)
				{
					while (oracleReader.Read())
					{
						tablesList.Add(oracleReader[0].ToString());
					}
				}
			}

			return tablesList;
		}

		#region Internal Implementations

		private static OracleConnection GetConnection()
		{
			var connstr = ConfigurationManager.ConnectionStrings["OracleConnection"];
			var connectionString = ConfigurationManager.ConnectionStrings["OracleConnection"].ConnectionString;
			return new OracleConnection(connectionString);
		}

		#endregion
	}
}

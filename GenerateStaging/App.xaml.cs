﻿using Autofac;
using GenerateStaging.DataAccess;
using GenerateStaging.ViewModels;
using GenerateStaging.Xsd;
using System.Windows;

namespace GenerateStaging
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static IContainer Container { get; set; }

		private void OnStartup(object sender, StartupEventArgs e)
		{
			RegisterDependencies();

			ShowMainView();
		}

		private void RegisterDependencies()
		{
			var builder = new ContainerBuilder();

			//builder.RegisterType<OracleDatabaseExplorer>().As<IDatabaseExplorer>();
			builder.RegisterType<MockDatabaseExplorer>().As<IDatabaseExplorer>();
			builder.RegisterType<SchemaGenerator>().As<ISchemaGenerator>();

			Container = builder.Build();
		}

		private void ShowMainView()
		{
			var mainView = new MainWindow();
			var mainViewModel = new MainViewModel(Container);
			mainView.DataContext = mainViewModel;
			mainView.Show();
		}

		private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			MessageBox.Show("An unhandled exception just occurred: " + e.Exception.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Warning);

			e.Handled = true;

			Shutdown();
		}
	}
}

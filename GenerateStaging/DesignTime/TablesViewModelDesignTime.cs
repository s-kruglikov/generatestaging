﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GenerateStaging.DesignTime
{
	public class TablesViewModelDesignTime
	{
		public TablesViewModelDesignTime()
		{
			DatabaseTables = new ObservableCollection<string>(new List<string>
			{
				"TABLE_NAME_1",
				"TABLE_NAME_2",
				"TABLE_NAME_3",
				"TABLE_NAME_4",
				"TABLE_NAME_5",
				"TABLE_NAME_6",
				"TABLE_NAME_7",
				"TABLE_NAME_8",
				"TABLE_NAME_9",
				"TABLE_NAME_10"
			});

			FilterExpression = "TABLE_";
			SelectedTable = "STG_TABLE_1";
		}

		public ObservableCollection<string> DatabaseTables { get; set; }

		public string SelectedTable { get; set; }

		public string FilterExpression { get; set; }
	}
}

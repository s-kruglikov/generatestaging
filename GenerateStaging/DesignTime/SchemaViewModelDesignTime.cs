﻿using GenerateStaging.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GenerateStaging.DesignTime
{
	public class SchemaViewModelDesignTime
	{
		public SchemaViewModelDesignTime()
		{
			SchemaTree = MockSchemaTree();

			SelectedSchemaItem = new SchemaItem(new Table("STG_TABLE_1"));

			TargetNamespace = @"http://www.schema.com";
		}

		public ObservableCollection<SchemaItem> SchemaTree { get; set; }

		public SchemaItem SelectedSchemaItem { get; set; }

		public string TargetNamespace { get; set; }

		private ObservableCollection<SchemaItem> MockSchemaTree()
		{
			var stgReceivedFile = new Table("STG_TABLE_1");
			stgReceivedFile.Columns = new List<Column>();

			var stgRisingBill = new Table();
			stgRisingBill.Name = "STG_TABLE_2";
			stgRisingBill.Columns = new List<Column>();

			var stgRisingDetail = new Table();
			stgRisingDetail.Name = "STG_TABLE_3";
			stgRisingDetail.Columns = new List<Column>();

			var stgRisingDiagnosis = new Table();
			stgRisingDiagnosis.Name = "STG_TABLE_4";
			stgRisingDiagnosis.Columns = new List<Column>();

			var result = new ObservableCollection<SchemaItem>();

			var receivedFileSchemaItem = new SchemaItem(stgReceivedFile);
			var risingBillSchemaItem = new SchemaItem(stgRisingBill);
			var risingDetailSchemaItem = new SchemaItem(stgRisingDetail);
			var risingDiagnosisSchemaItem = new SchemaItem(stgRisingDiagnosis);
			risingDiagnosisSchemaItem.IsSelected = true;

			risingBillSchemaItem.Children.Add(risingDetailSchemaItem);
			risingBillSchemaItem.Children.Add(risingDiagnosisSchemaItem);

			receivedFileSchemaItem.Children.Add(risingBillSchemaItem);

			result.Add(receivedFileSchemaItem);

			return result;
		}
	}
}

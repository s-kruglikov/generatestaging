﻿namespace GenerateStaging.DesignTime
{
	public class MainViewModelDesignTime
	{
		public TablesViewModelDesignTime TablesViewModel
		{
			get
			{
				return new TablesViewModelDesignTime();
			}
		}

		public SchemaViewModelDesignTime SchemaViewModel
		{
			get
			{
				return new SchemaViewModelDesignTime();
			}
		}
	}
}

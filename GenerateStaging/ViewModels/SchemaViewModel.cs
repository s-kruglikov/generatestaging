﻿#region Usings

using GenerateStaging.Abstract;
using GenerateStaging.Commands;
using GenerateStaging.Models;
using GenerateStaging.Xsd;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;

#endregion

namespace GenerateStaging.ViewModels
{
	public class SchemaViewModel : ViewModelBase, ISchemaViewModel
	{
		#region Private Fields

		private readonly ISchemaGenerator _schemaGenerator;
		private ObservableCollection<SchemaItem> _schemaTree;
		private SchemaItem _selectedSchemaItem;
		private string _targetNamespace;
		private ICommand _clearSchemaTreeCommand;
		private ICommand _generateSchemaCommand;
		private ICommand _moveUpSchemaItemCommand;
		private ICommand _moveDownSchemaItemCommand;
		private ICommand _deleteSchemaItemCommand;

		#endregion

		#region Constants

		private const string InitialTargetNamespace = "http://www.schema.com";
		private const string DefaultFileName = "Staging.xsd";
		private const string DefaultFilter = "XSD Schema (*.xsd)|*.xsd";

		#endregion

		#region ISchemaViewModel implementation

		public ObservableCollection<SchemaItem> SchemaTree
		{
			get
			{
				return _schemaTree;
			}
			set
			{
				_schemaTree = value;
				OnPropertyChanged("SchemaTree");
			}
		}

		public SchemaItem SelectedSchemaItem
		{
			get
			{
				FindSchemaItem(
					SchemaTree,
					(item) => { return item.IsSelected; },
					(item) => { _selectedSchemaItem = item; }
				);
				return _selectedSchemaItem;
			}
			set
			{
				_selectedSchemaItem = value;
				OnPropertyChanged("SelectedSchemaItem");
			}
		}

		public string TargetNamespace
		{
			get
			{
				return _targetNamespace;
			}
			set
			{
				_targetNamespace = value;
				OnPropertyChanged("TargetNamespace");
			}
		}

		public void AddChildSchemaItem(Table table)
		{
			var updatedSchemaTree = SchemaTree;

			var schemaItem = new SchemaItem(table)
			{
				IsSelected = true
			};

			// if no elements add as root
			if (updatedSchemaTree.Count == 0)
			{
				updatedSchemaTree.Add(schemaItem);
			}

			else
			{
				// add schema item as a child to selected
				FindSchemaItem(
					updatedSchemaTree,
					(item) => { return item.IsSelected == true; },
					(item) => { item.Children.Add(new SchemaItem(table) { Parent = item, IsSelected = true }); }
				);
			}

			SchemaTree = new ObservableCollection<SchemaItem>(updatedSchemaTree);
		}

		public void AddSiblingSchemaItem(Table table)
		{
			// add schema item as a sibling to selected
			var parent = SelectedSchemaItem.Parent;

			var schemaItem = new SchemaItem(table)
			{
				IsSelected = true,
				Parent = parent
			};

			parent.Children.Add(schemaItem);

			// Rebuild schema tree
			SchemaTree = new ObservableCollection<SchemaItem>(SchemaTree);
		}

		public ICommand ClearSchemaTreeCommand
		{
			get
			{
				if (_clearSchemaTreeCommand == null)
				{
					_clearSchemaTreeCommand = new DelegateCommand(
						obj => CanClearSchemaTree(),
						obj => ClearSchemaTree());
				}

				return _clearSchemaTreeCommand;
			}
		}

		public ICommand GenerateSchemaCommand
		{
			get
			{
				if (_generateSchemaCommand == null)
				{
					_generateSchemaCommand = new DelegateCommand(
						obj => CanGenerateSchema(),
						obj => GenerateSchema());
				}

				return _generateSchemaCommand;
			}
		}

		public ICommand MoveUpSchemaItemCommand
		{
			get
			{
				if (_moveUpSchemaItemCommand == null)
				{
					_moveUpSchemaItemCommand = new DelegateCommand(
						obj => CanMoveUpSchemaItem(),
						obj => MoveUpSchemaItem());
				}
				return _moveUpSchemaItemCommand;
			}
		}

		public ICommand MoveDownSchemaItemCommand
		{
			get
			{
				if(_moveDownSchemaItemCommand == null)
				{
					_moveDownSchemaItemCommand = new DelegateCommand(
						obj => CanMoveDownSchemaItem(),
						obj => MoveDownSchemaItem());
				}

				return _moveDownSchemaItemCommand;
			}
		}

		public ICommand DeleteSchemaItemCommand
		{
			get
			{
				if (_deleteSchemaItemCommand == null)
				{
					_deleteSchemaItemCommand = new DelegateCommand(
						obj => CanDeleteSchemaItem(),
						obj => DeleteSchemaItem());
				}

				return _deleteSchemaItemCommand;
			}
		}

		#endregion

		#region Constructor

		public SchemaViewModel(ISchemaGenerator schemaGenerator)
		{
			_schemaGenerator = schemaGenerator;

			SchemaTree = new ObservableCollection<SchemaItem>();
			TargetNamespace = InitialTargetNamespace;
		}

		#endregion

		#region Internal Implementations

		private void FindSchemaItem(
			ICollection<SchemaItem> items,
			Func<SchemaItem, bool> filter,
			Action<SchemaItem> action)
		{
			foreach (var item in items)
			{
				if (filter(item) == true)
				{
					action(item);
				}
				else if (item.Children.Count > 0)
				{
					FindSchemaItem(item.Children, filter, action);
				}
			}
		}

		private bool CanClearSchemaTree()
		{
			return SchemaTree.Count > 0;
		}

		private void ClearSchemaTree()
		{
			SchemaTree = new ObservableCollection<SchemaItem>();
		}

		private bool CanGenerateSchema()
		{
			return SchemaTree.Count > 0;
		}

		private void GenerateSchema()
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				FileName = DefaultFileName,
				Filter = DefaultFilter
			};

			if (saveFileDialog.ShowDialog() == true)
			{
				string fileName = saveFileDialog.FileName;

				if (File.Exists(fileName))
					File.Delete(fileName);

				using (var resultStream = new FileStream(fileName, FileMode.CreateNew))
				{
					_schemaGenerator.Generate(resultStream, SchemaTree[0], TargetNamespace);

					resultStream.Flush();
				}

				Process.Start("explorer.exe", Path.GetDirectoryName(saveFileDialog.FileName));
			}
		}

		private bool CanMoveUpSchemaItem()
		{
			if (SelectedSchemaItem != null)
			{
				if (SelectedSchemaItem.Parent != null && SelectedSchemaItem.Parent.Children.Count > 0)
				{
					if (SelectedSchemaItem.Parent.Children.IndexOf(SelectedSchemaItem) > 0)
					{
						return true;
					}
				}
			}
			return false;
		}

		private void MoveUpSchemaItem()
		{
			var parent = SelectedSchemaItem.Parent;
			var itemIndex = parent.Children.IndexOf(SelectedSchemaItem);

			parent.Children.RemoveAt(itemIndex);
			parent.Children.Insert(itemIndex - 1, SelectedSchemaItem);

			// Rebuild schema tree
			SchemaTree = new ObservableCollection<SchemaItem>(SchemaTree);
		}

		private bool CanMoveDownSchemaItem()
		{
			if (SelectedSchemaItem != null)
			{
				if (SelectedSchemaItem.Parent != null && SelectedSchemaItem.Parent.Children.Count > 0)
				{
					if (SelectedSchemaItem.Parent.Children.IndexOf(SelectedSchemaItem) < SelectedSchemaItem.Parent.Children.Count - 1)
					{
						return true;
					}
				}
			}
			return false;
		}

		private void MoveDownSchemaItem()
		{
			var parent = SelectedSchemaItem.Parent;
			var itemIndex = parent.Children.IndexOf(SelectedSchemaItem);

			parent.Children.RemoveAt(itemIndex);
			parent.Children.Insert(itemIndex + 1, SelectedSchemaItem);

			// Rebuild schema tree
			SchemaTree = new ObservableCollection<SchemaItem>(SchemaTree);
		}

		private bool CanDeleteSchemaItem()
		{
			return SelectedSchemaItem != null;
		}

		private void DeleteSchemaItem()
		{
			var parent = SelectedSchemaItem.Parent;

			if (parent != null)
			{
				SelectedSchemaItem.Parent.Children.Remove(SelectedSchemaItem);
				parent.IsSelected = true;
			}
			else
				SchemaTree = new ObservableCollection<SchemaItem>();

			// Rebuild schema tree
			SchemaTree = new ObservableCollection<SchemaItem>(SchemaTree);
		}

		#endregion
	}
}

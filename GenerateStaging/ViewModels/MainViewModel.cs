﻿#region Usings

using Autofac;
using GenerateStaging.Abstract;
using GenerateStaging.Commands;
using GenerateStaging.DataAccess;
using GenerateStaging.Models;
using GenerateStaging.Xsd;
using System.Linq;
using System.Windows.Input;

#endregion

namespace GenerateStaging.ViewModels
{
	public class MainViewModel : ViewModelBase, IMainViewModel
	{
		#region Private Fieds

		// Child viewmodels

		// Services
		private readonly IDatabaseExplorer _databaseExplorer;
		private readonly ISchemaGenerator _schemaGenerator;

		// Commands
		private ICommand _addChildCommand;
		private ICommand _addSiblingCommand;

		#endregion

		#region IMainViewModel implementation

		public MainViewModel(IContainer dependenciesContainer)
		{
			// Resolve services
			_databaseExplorer = dependenciesContainer.Resolve<IDatabaseExplorer>();
			_schemaGenerator = dependenciesContainer.Resolve<ISchemaGenerator>();

			TablesViewModel = new TablesViewModel(_databaseExplorer);
			SchemaViewModel = new SchemaViewModel(_schemaGenerator);
		}

		public ITablesViewModel TablesViewModel { get; }

		public ISchemaViewModel SchemaViewModel { get; }

		public ICommand AddChildCommand
		{
			get
			{
				if(_addChildCommand == null)
					_addChildCommand = new DelegateCommand(
						obj => CanAddChild(),
						obj => AddChild()
					);

				return _addChildCommand;
			}
		}

		public ICommand AddSiblingCommand
		{
			get
			{
				if(_addSiblingCommand == null)
				{
					_addSiblingCommand = new DelegateCommand(
						obj => CanAddSibling(),
						obj => AddSibling());
				}

				return _addSiblingCommand;
			}
		}

		#endregion

		#region Internal Implementations

		private bool CanAddChild()
		{
			return !string.IsNullOrEmpty(TablesViewModel.SelectedTable);
		}

		private void AddChild()
		{
			var table = GetTable();
			SchemaViewModel.AddChildSchemaItem(table);
		}

		private bool CanAddSibling()
		{
			return !string.IsNullOrEmpty(TablesViewModel.SelectedTable) 
					&& SchemaViewModel.SelectedSchemaItem != null
					&& SchemaViewModel.SelectedSchemaItem != SchemaViewModel.SchemaTree[0]; // no siblings to root element
		}

		private void AddSibling()
		{
			var table = GetTable();
			SchemaViewModel.AddSiblingSchemaItem(table);
		}

		private Table GetTable()
		{
			return new Table(TablesViewModel.SelectedTable)
			{
				Columns = _databaseExplorer
					.GetColumns(TablesViewModel.SelectedTable)
					.Select(col => new Column(col))
			};
		}

		#endregion
	}
}

﻿#region Usings

using GenerateStaging.Abstract;
using GenerateStaging.Commands;
using GenerateStaging.DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

#endregion

namespace GenerateStaging.ViewModels
{
	public class TablesViewModel : ViewModelBase, ITablesViewModel
	{
		#region Private Fields

		private readonly IDatabaseExplorer _databaseExplorer;
		private List<string> _initialTablesList;
		private ObservableCollection<string> _databaseTables;
		private string _selectedTable;
		private string _filterExpression;
		private ICommand _clearFilterCommand;

		#endregion

		#region Events

		public event EventHandler DataLoaded;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="databaseExplorer"></param>
		public TablesViewModel(IDatabaseExplorer databaseExplorer)
		{
			_databaseExplorer = databaseExplorer;
		}

		#endregion

		#region ITablesViewModel Implementation

		/// <summary>
		/// Represents collection of table names stored in the database.
		/// </summary>
		public ObservableCollection<string> DatabaseTables {
			get
			{
				return _databaseTables;
			}
			set
			{
				_databaseTables = value;
				OnPropertyChanged("DatabaseTables");
			}
		}

		/// <summary>
		/// Fills databa tables collection.
		/// </summary>
		public void LoadDatabaseTables()
		{
			var backgroundWorker = new BackgroundWorker();

			backgroundWorker.DoWork += (sender, e) =>
			{
				_initialTablesList = _databaseExplorer.GetTables();
			};

			backgroundWorker.RunWorkerCompleted += (sender, e) =>
			{
				if(e.Error != null)
				{
					throw e.Error;
				}

				DatabaseTables = new ObservableCollection<string>(_initialTablesList);
				if (DataLoaded != null)
				{
					DataLoaded(this, null);
				}
			};

			backgroundWorker.RunWorkerAsync();
		}

		/// <summary>
		/// Reresents currently selected table
		/// </summary>
		public string SelectedTable
		{
			get
			{
				return _selectedTable;
			}
			set
			{
				_selectedTable = value;
				OnPropertyChanged("SelectedTable");
			}
		}

		/// <summary>
		/// Represents tables search filter expression
		/// </summary>
		public string FilterExpression
		{
			get
			{
				return _filterExpression;
			}
			set
			{
				_filterExpression = value;

				if (string.IsNullOrEmpty(value))
					DatabaseTables = new ObservableCollection<string>(_initialTablesList);
				else
				{
					DatabaseTables =
					new ObservableCollection<string>(
						_initialTablesList.Where(f => f.StartsWith(_filterExpression, StringComparison.OrdinalIgnoreCase)));
				}

				OnPropertyChanged("FilterExpression");
			}
		}

		/// <summary>
		/// Clear table filter expression command
		/// </summary>
		public ICommand ClearFilterCommand
		{
			get
			{
				if (_clearFilterCommand == null)
					_clearFilterCommand = new DelegateCommand(
						obj => CanClearFilter(),
						obj => ClearFilter());

				return _clearFilterCommand;
			}
		}

		#endregion

		#region Internal Implementations

		private bool CanClearFilter()
		{
			return !string.IsNullOrEmpty(FilterExpression);
		}

		private void ClearFilter()
		{
			FilterExpression = string.Empty;
		}

		#endregion
	}
}

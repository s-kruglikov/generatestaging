﻿using GenerateStaging.ViewModels;
using System.Windows;

namespace GenerateStaging
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string _minOccurs;
		private string _maxOccurs;

		private const string MinOccursValidationMessage = @"The value of Min Occurs must be a positive integer.";
		private const string MaxOccursValidationMessage = @"The value of Max Occurs must be a positive integer or the literal string 'unbounded'.";
		private const string UnboundedCharacter = @"*";
		private const string UnboundedString = @"unbounded";

		public MainWindow()
		{
			InitializeComponent();
		}

		private void ShowLoadingAnimation()
		{
			LoadingAnimation.Visibility = Visibility.Visible;

			// Hide controls
			TablesView.Visibility = Visibility.Hidden;
			AddButtons.Visibility = Visibility.Hidden;
			SchemaView.Visibility = Visibility.Hidden;
			Properties.Visibility = Visibility.Hidden;
			SchemaButtons.Visibility = Visibility.Hidden;
			Status.Visibility = Visibility.Hidden;
		}

		private void ShowControls()
		{
			LoadingAnimation.Visibility = Visibility.Hidden;

			// Show controls
			TablesView.Visibility = Visibility.Visible;
			AddButtons.Visibility = Visibility.Visible;
			SchemaView.Visibility = Visibility.Visible;
			SchemaButtons.Visibility = Visibility.Visible;
			Status.Visibility = Visibility.Visible;
		}

		private void SchemaTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			var mainViewModel = (MainViewModel)DataContext;

			if (mainViewModel.SchemaViewModel.SchemaTree.Count > 0)
			{
				Properties.Visibility = Visibility.Visible;
			}
			else
			{
				Properties.Visibility = Visibility.Hidden;
			}

			// Re-evaluate selected schema item
			mainViewModel.SchemaViewModel.SelectedSchemaItem = null;

			if(mainViewModel.SchemaViewModel.SelectedSchemaItem != null)
			{
				_minOccurs = mainViewModel.SchemaViewModel.SelectedSchemaItem.MinOccurs;
				_maxOccurs = mainViewModel.SchemaViewModel.SelectedSchemaItem.MaxOccurs;
			}
			else
			{
				_minOccurs = string.Empty;
				_maxOccurs = string.Empty;
			}
		}

		private void TxtMinOccurs_LostFocus(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txtMinOccurs.Text))
			{
				_minOccurs = string.Empty;
			}
			else
			{
				try
				{
					var num = int.Parse(txtMinOccurs.Text);
					_minOccurs = num.ToString();
				}
				catch
				{
					txtMinOccurs.Text = _minOccurs;
					MessageBox.Show(MinOccursValidationMessage);
					return;
				}
			}

			txtMinOccurs.Text = _minOccurs;
		}

		private void TxtMaxOccurs_LostFocus(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txtMaxOccurs.Text))
			{
				_maxOccurs = string.Empty;
			}
			else if (txtMaxOccurs.Text.ToLowerInvariant() == UnboundedString
				|| txtMaxOccurs.Text.ToLowerInvariant() == UnboundedCharacter)
			{
				_maxOccurs = UnboundedString;
			}
			else
			{
				try
				{
					var num = int.Parse(txtMaxOccurs.Text);
					_maxOccurs = num.ToString();
				}
				catch
				{
					txtMaxOccurs.Text = _maxOccurs;
					MessageBox.Show(MaxOccursValidationMessage);
					return;
				}
			}

			txtMaxOccurs.Text = _maxOccurs;
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			ShowLoadingAnimation();

			var mainViewModel = (MainViewModel)DataContext;

			// Show controls after tables loaded.
			mainViewModel.TablesViewModel.DataLoaded += (s, ea) => 
			{
				ShowControls();
			};

			mainViewModel.TablesViewModel.LoadDatabaseTables();
		}
	}
}

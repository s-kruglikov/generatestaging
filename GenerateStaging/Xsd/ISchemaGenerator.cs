﻿using GenerateStaging.Models;
using System.IO;

namespace GenerateStaging.Xsd
{
	public interface ISchemaGenerator
	{
		void Generate(Stream destinationStream, SchemaItem tablesTree, string targetNamespace);
	}
}

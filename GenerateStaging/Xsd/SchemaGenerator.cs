﻿using GenerateStaging.Models;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace GenerateStaging.Xsd
{
	public class SchemaGenerator : ISchemaGenerator
	{
		#region Constants

		private const string biztalkNamespace = "http://schemas.microsoft.com/BizTalk/2003";
		private const string xsdNamespace = "http://www.w3.org/2001/XMLSchema";

		#endregion

		#region Public Interface

		public void Generate(Stream destinationStream, SchemaItem schemaTree, string targetNamespace)
		{
			var schema = InitSchema(targetNamespace);

			XmlSchemaElement transactionElement = new XmlSchemaElement();
			XmlSchemaComplexType transactionType = new XmlSchemaComplexType();
			XmlSchemaSequence transactionSequence = new XmlSchemaSequence();
			transactionSequence.Items.Add(GetTableElement(schemaTree));

			transactionType.Particle = transactionSequence;
			transactionElement.SchemaType = transactionType;
			transactionElement.Name = "Transaction";

			schema.Items.Add(transactionElement);

			var writerSettings = new XmlWriterSettings()
			{
				Encoding = Encoding.Unicode,
				Indent = true
			};

			using (var writer = XmlWriter.Create(destinationStream, writerSettings))
			{
				schema.Write(writer);
				writer.Flush();
			}
		}

		#endregion

		#region Internal Implementations

		private XmlSchema InitSchema(string targetNamespace)
		{
			XmlSchema stagingSchema = new XmlSchema
			{
				TargetNamespace = targetNamespace
			};

			XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
			namespaces.Add("", targetNamespace);
			namespaces.Add("b", biztalkNamespace);

			stagingSchema.Namespaces = namespaces;

			return stagingSchema;
		}

		/// <summary>
		/// Create Columns element
		/// </summary>
		/// <param name="columns"></param>
		/// <returns></returns>
		private XmlSchemaElement GetColumnsElement(IEnumerable<Column> columns)
		{
			XmlSchemaComplexType columnsType = new XmlSchemaComplexType();
			XmlSchemaSequence columnsSequence = new XmlSchemaSequence();
			foreach (var column in columns)
			{
				XmlSchemaElement columnField = new XmlSchemaElement();
				columnField.Name = column.Name;
				columnField.SchemaTypeName = new XmlQualifiedName("string", xsdNamespace);

				columnsSequence.Items.Add(columnField);
			}

			XmlSchemaElement columnsElement = new XmlSchemaElement();
			columnsType.Particle = columnsSequence;
			columnsElement.SchemaType = columnsType;
			columnsElement.Name = "Columns";

			return columnsElement;
		}

		/// <summary>
		/// Create element for table
		/// </summary>
		/// <param name="schemaItem"></param>
		/// <returns></returns>
		private XmlSchemaElement GetTableElement(SchemaItem schemaItem)
		{
			var columnsElement = GetColumnsElement(schemaItem.Table.Columns);

			XmlSchemaComplexType stgTableType = new XmlSchemaComplexType();
			XmlSchemaSequence stgTableSequence = new XmlSchemaSequence();
			stgTableSequence.Items.Add(columnsElement);

			if (schemaItem.Children != null)
			{
				foreach (var child in schemaItem.Children)
				{
					stgTableSequence.Items.Add(GetTableElement(child));
				}
			}

			XmlSchemaElement tableElement = new XmlSchemaElement();
			stgTableType.Particle = stgTableSequence;
			tableElement.SchemaType = stgTableType;
			tableElement.Name = schemaItem.Table.Name;

			if(!string.IsNullOrEmpty(schemaItem.MinOccurs))
				tableElement.MinOccursString = schemaItem.MinOccurs;

			if(!string.IsNullOrEmpty(schemaItem.MaxOccurs))
				tableElement.MaxOccursString = schemaItem.MaxOccurs;

			return tableElement;
		}

		#endregion
	}
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace GenerateStaging.Converters
{
	public class SelectedDatabaseTableToStatusConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return string.Empty;
			else
				return string.Format("Database table: '{0}' | ", value.ToString());
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}

﻿namespace GenerateStaging.Models
{
	public class Column
	{
		public string Name { get; set; }

		public Column(string name)
		{
			Name = name;
		}
	}
}

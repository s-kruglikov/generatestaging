﻿using System.Collections.Generic;

namespace GenerateStaging.Models
{
	/// <summary>
	/// Represents entry in the schema tree.
	/// </summary>
	public class SchemaItem
	{
		/// <summary>
		/// Table from the database
		/// </summary>
		public Table Table { get; private set; }

		/// <summary>
		/// Min occurence of the element in the schema.
		/// </summary>
		public string MinOccurs { get; set; }

		/// <summary>
		/// Max occurence of the element in the schema.
		/// </summary>
		public string MaxOccurs { get; set; }

		/// <summary>
		/// Element is expanded in the tree.
		/// </summary>
		public bool IsExpanded { get; set; }

		/// <summary>
		/// Element is selected.
		/// </summary>
		public bool IsSelected { get; set; }

		/// <summary>
		/// Children elements.
		/// </summary>
		public IList<SchemaItem> Children { get; set; }

		/// <summary>
		/// Stores reference to parent element.
		/// </summary>
		public SchemaItem Parent { get; set; }

		/// <summary>
		/// Constructs new instance of the SchemaItem.
		/// </summary>
		/// <param name="table"></param>
		public SchemaItem(Table table)
		{
			Table = table;
			MinOccurs = string.Empty;
			MaxOccurs = string.Empty;
			Children = new List<SchemaItem>();
			IsExpanded = true;
		}
	}
}

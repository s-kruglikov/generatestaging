﻿using System.Collections.Generic;

namespace GenerateStaging.Models
{
	/// <summary>
	/// Represents table stored in the database.
	/// </summary>
	public class Table
	{
		/// <summary>
		/// Table name.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Table columns.
		/// </summary>
		public IEnumerable<Column> Columns { get; set; }

		#region Constructors

		public Table() : this(string.Empty)
		{

		}

		public Table(string name)
		{
			Name = name;
			Columns = new List<Column>();
		}

		#endregion
	}
}

﻿using System.Windows.Input;

namespace GenerateStaging.Abstract
{
	public interface IMainViewModel
	{
		ITablesViewModel TablesViewModel { get; }

		ISchemaViewModel SchemaViewModel { get; }

		ICommand AddChildCommand { get; }

		ICommand AddSiblingCommand { get; }
	}
}

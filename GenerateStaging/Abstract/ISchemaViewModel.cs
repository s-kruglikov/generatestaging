﻿using GenerateStaging.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace GenerateStaging.Abstract
{
	public interface ISchemaViewModel
	{
		ObservableCollection<SchemaItem> SchemaTree { get; set; }

		void AddChildSchemaItem(Table table);

		void AddSiblingSchemaItem(Table table);

		SchemaItem SelectedSchemaItem { get; set; }

		string TargetNamespace { get; set; }

		ICommand ClearSchemaTreeCommand { get; }

		ICommand GenerateSchemaCommand { get; }

		ICommand MoveUpSchemaItemCommand { get; }

		ICommand MoveDownSchemaItemCommand { get; }

		ICommand DeleteSchemaItemCommand { get; }
	}
}

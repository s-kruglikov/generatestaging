﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace GenerateStaging.Abstract
{
	public interface ITablesViewModel
	{
		event EventHandler DataLoaded;

		ObservableCollection<string> DatabaseTables { get; set; }

		void LoadDatabaseTables();

		string SelectedTable { get; set; }

		string FilterExpression { get; set; }

		ICommand ClearFilterCommand { get; }
	}
}

# Summary #
WFP application to retrieve list of tables stored in the database, create hierarchical structure representing dependencies between tables and automatically generate xsd schema for this structure.
A tool to EDI solution.

# Features #
+	Retrieve the list of tables stored in database schema (uses Oracle.ManagedDataAccess lib);
+	Filter tables by name;
+	Add table as child or as a sibling to hierarchical structure which represents schema;
+	Specify schema's target namespace;
+	Set minOccurence and maxOccurence attributes for each table in schema tree;
+	Automatically retrieve each table�s columns and form xsd schema valid for further development.

# Scheenshot #
![Screenshot](https://bitbucket.org/s-kruglikov/generatestaging/raw/3229f774f23d27e4f6342a542df5bb3d76d43fb9/Screenshot.png)

## Dependencies ##
- .NET framework 4.0

## Configuration ##
- Update Oracle connection string:

```/configuration/oracle.manageddataaccess.client/version/dataSources/dataSource[@descriptor]```

- Set User Id and Password:

```/configuration/connectionStrings/add[@connectionString]```
